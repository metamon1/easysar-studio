#ifndef MOCKRTE_H
#define MOCKRTE_H

/*********************************************************************************************************************
* Includes
*********************************************************************************************************************/
#include "Std_Types.h"
#include "Rte_Type.h"
#include "Rte.h"

/*********************************************************************************************************************
* Constants and Types
*********************************************************************************************************************/

/*********************************************************************************************************************
* Public Function Declarations
*********************************************************************************************************************/

void Rte_Start(void);
OnOff_T Rte_GetWriteData_Telltale_TelltaleAutoCruiseStatusPort_TelltaleAutoCruiseStatus(void);
OnOff_T Rte_GetWriteData_Telltale_TelltaleHighBeamStatusPort_TelltaleHighBeamStatus(void);
OnOff_T Rte_GetWriteData_Telltale_TelltaleTPMSStatusPort_TelltaleTPMSStatus(void);


#endif //MOCKRTE_H


