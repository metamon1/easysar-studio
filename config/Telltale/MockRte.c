/*********************************************************************************************************************
* Includes
*********************************************************************************************************************/
#include "MockRte.h"

/*********************************************************************************************************************
* Constants and Types
*********************************************************************************************************************/

/*********************************************************************************************************************
* Local Variables
*********************************************************************************************************************/
static OnOff_T Telltale_TelltaleAutoCruiseStatusPort_TelltaleAutoCruiseStatus;
static OnOff_T Telltale_TelltaleHighBeamStatusPort_TelltaleHighBeamStatus;
static OnOff_T Telltale_TelltaleTPMSStatusPort_TelltaleTPMSStatus;

/*********************************************************************************************************************
* Public Functions
*********************************************************************************************************************/
void Rte_Start(void)
{
   Telltale_TelltaleAutoCruiseStatusPort_TelltaleAutoCruiseStatus = 0;
   Telltale_TelltaleHighBeamStatusPort_TelltaleHighBeamStatus = 0;
   Telltale_TelltaleTPMSStatusPort_TelltaleTPMSStatus = 0;
}

Std_ReturnType Rte_Write_Telltale_TelltaleAutoCruiseStatusPort_TelltaleAutoCruiseStatus(OnOff_T data)
{
   Telltale_TelltaleAutoCruiseStatusPort_TelltaleAutoCruiseStatus = data;
   return RTE_E_OK;
}

Std_ReturnType Rte_Write_Telltale_TelltaleHighBeamStatusPort_TelltaleHighBeamStatus(OnOff_T data)
{
   Telltale_TelltaleHighBeamStatusPort_TelltaleHighBeamStatus = data;
   return RTE_E_OK;
}

Std_ReturnType Rte_Write_Telltale_TelltaleTPMSStatusPort_TelltaleTPMSStatus(OnOff_T data)
{
   Telltale_TelltaleTPMSStatusPort_TelltaleTPMSStatus = data;
   return RTE_E_OK;
}

OnOff_T Rte_GetWriteData_Telltale_TelltaleAutoCruiseStatusPort_TelltaleAutoCruiseStatus(void)
{
   return Telltale_TelltaleAutoCruiseStatusPort_TelltaleAutoCruiseStatus;
}

OnOff_T Rte_GetWriteData_Telltale_TelltaleHighBeamStatusPort_TelltaleHighBeamStatus(void)
{
   return Telltale_TelltaleHighBeamStatusPort_TelltaleHighBeamStatus;
}

OnOff_T Rte_GetWriteData_Telltale_TelltaleTPMSStatusPort_TelltaleTPMSStatus(void)
{
   return Telltale_TelltaleTPMSStatusPort_TelltaleTPMSStatus;
}

