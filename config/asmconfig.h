#ifndef _AS_MCONF_H_

#ifndef USE_ECUM
#define USE_ECUM 1
#endif

#ifndef USE_SCHM
#define USE_SCHM 1
#endif

#ifndef USE_MCU
#define USE_MCU 1
#endif

#ifndef USE_DET
#define USE_DET 1
#endif

#ifndef USE_DEM
#define USE_DEM 1
#endif

#ifndef USE_SHELL
#define USE_SHELL 1
#endif

#ifndef USE_CAN
#define USE_CAN 1
#endif

#ifndef USE_CANIF
#define USE_CANIF 1
#endif

#ifndef USE_PDUR
#define USE_PDUR 1
#endif

#ifndef USE_COM
#define USE_COM 1
#endif

#ifndef USE_COMM
#define USE_COMM 1
#endif

#ifndef USE_CANTP
#define USE_CANTP 1
#endif

#ifndef USE_LIN
#define USE_LIN 1
#endif

#ifndef USE_LINIF
#define USE_LINIF 1
#endif

#ifndef USE_LINTP
#define USE_LINTP 1
#endif

#ifndef USE_LINTPGW
#define USE_LINTPGW 1
#endif

#ifndef USE_DCM
#define USE_DCM 1
#endif

#ifndef USE_CANNM
#define USE_CANNM 1
#endif

#ifndef USE_CANSM
#define USE_CANSM 1
#endif

#ifndef USE_NM
#define USE_NM 1
#endif

#ifndef USE_OSEKNM
#define USE_OSEKNM 1
#endif

#ifndef USE_XCP
#define USE_XCP 1
#endif

#ifndef USE_PORT
#define USE_PORT 1
#endif

#ifndef USE_DIO
#define USE_DIO 1
#endif

#ifndef USE_EEP
#define USE_EEP 1
#endif

#ifndef USE_FLS
#define USE_FLS 1
#endif

#ifndef USE_STMO
#define USE_STMO 1
#endif

#ifndef USE_EA
#define USE_EA 1
#endif

#ifndef USE_FEE
#define USE_FEE 1
#endif

#ifndef USE_MEMIF
#define USE_MEMIF 1
#endif

#ifndef USE_NVM
#define USE_NVM 1
#endif

#ifndef USE_LCD
#define USE_LCD 1
#endif

#ifndef USE_TINYOS
#define USE_TINYOS 1
#endif

#ifndef USE_J1939TP
#define USE_J1939TP 1
#endif

#ifndef USE_TRACE
#define USE_TRACE 1
#endif

#ifndef USE_COMMONXML
#define USE_COMMONXML 1
#endif

#ifndef USE_AWS
#define USE_AWS 1
#endif

#ifndef USE_SOAD
#define USE_SOAD 1
#endif

#ifndef USE_DOIP
#define USE_DOIP 1
#endif

#ifndef USE_LWIP
#define USE_LWIP 1
#endif

#ifndef USE_LWIP_POSIX_ARCH
#define USE_LWIP_POSIX_ARCH 1
#endif

#ifndef USE_SD
#define USE_SD 1
#endif

#ifndef USE_FATFS
#define USE_FATFS 1
#endif

#ifndef USE_LWEXT4
#define USE_LWEXT4 1
#endif

#ifndef USE_VFS
#define USE_VFS 1
#endif

#ifndef USE_FTP
#define USE_FTP 1
#endif

#ifndef USE_DEV
#define USE_DEV 1
#endif

#ifndef USE_CLIB_MBOX
#define USE_CLIB_MBOX 1
#endif

#ifndef USE_CLIB_STDIO_PRINTF
#define USE_CLIB_STDIO_PRINTF 1
#endif

#ifndef USE_CLIB_STDIO_CAN
#define USE_CLIB_STDIO_CAN 1
#endif

#ifndef USE_RTE
#define USE_RTE 1
#endif

#ifndef USE_GTK
#define USE_GTK 1
#endif

#ifndef USE_SG
#define USE_SG 1
#endif

#ifndef USE_RTE_SWC_TELLTALE
#define USE_RTE_SWC_TELLTALE 1
#endif

#ifndef USE_RTE_SWC_GAUGE
#define USE_RTE_SWC_GAUGE 1
#endif

#ifndef USE_SG_DEMO_CLUSTER
#define USE_SG_DEMO_CLUSTER 1
#endif

#ifndef USE_RTE_SWC_LINAPP
#define USE_RTE_SWC_LINAPP 1
#endif

#ifndef USE_LUA_CAN
#define USE_LUA_CAN 1
#endif

#ifndef USE_LUA_SOCKET_CAN
#define USE_LUA_SOCKET_CAN 1
#endif

#ifndef USE_LUA_DEV
#define USE_LUA_DEV 1
#endif

#ifndef USE_LUA_DEV_LIN
#define USE_LUA_DEV_LIN 1
#endif

#ifndef USE_LUA_DEV_LIN_SOCKET
#define USE_LUA_DEV_LIN_SOCKET 1
#endif

#ifndef USE_LVGL
#define USE_LVGL 1
#endif

#ifndef USE_FLASH_CMD
#define USE_FLASH_CMD 1
#endif

#ifndef USE_FLASH
#define USE_FLASH 1
#endif

#ifndef USE_LUA_DEV_RS232
#define USE_LUA_DEV_RS232 1
#endif

#if defined(USE_RTTHREAD) && defined(USE_ARCH_X86)
#define _EXFUN(N,P) N P
#endif
#ifndef OS_TICKS_PER_SECOND
#define OS_TICKS_PER_SECOND 1000000
#endif
#ifndef FTPD_DEBUG
#define FTPD_DEBUG
#endif
#ifndef CONFIG_USE_DEFAULT_CFG
#define CONFIG_USE_DEFAULT_CFG
#endif
#ifndef CONFIG_HAVE_OWN_OFLAGS
#define CONFIG_HAVE_OWN_OFLAGS 0
#endif
#ifndef __AS_CAN_BUS__
#define __AS_CAN_BUS__
#endif
#ifndef CAN_LL_DL
#define CAN_LL_DL 64
#endif
#ifndef __AS_PY_DEV__
#define __AS_PY_DEV__
#endif
#ifndef __LINUX__
#define __LINUX__
#endif
#ifndef USE_AWS
#define USE_AWS
#endif
#endif /* _AS_MCONF_H_ */
