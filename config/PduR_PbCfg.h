/*
* Configuration of module: PduR
*
* Created by:   parai
* Copyright:    (C)parai@foxmail.com  
*
* Configured for (MCU):    posix
*
* Module vendor:           ArcCore
* Generator version:       01.00
*
* Generated by easySAR Studio (https://github.com/parai/as)
*/

#ifndef PDUR_PB_CFG_H_H
#define PDUR_PB_CFG_H_H
#if !(((PDUR_SW_MAJOR_VERSION == 2) && (PDUR_SW_MINOR_VERSION == 0)) )
#error PduR: Configuration file expected BSW module version to be 2.0.*
#endif
#if defined(USE_DCM)
#include "Dcm.h"
#endif
#if defined(USE_COM)
#include "Com.h"
#endif
#if defined(USE_CANIF)
#include "CanIf.h"
#endif
#if defined(USE_CANTP)
#include "CanTp.h"
#endif
#if defined(USE_SOAD)
#include "SoAd.h"
#endif
#if defined(USE_LINTP)
#include "LinTp.h"
#endif
#if defined(USE_LINSTP)
#include "LinSTp.h"
#endif

extern const PduR_PBConfigType PduR_Config;

#if(PDUR_ZERO_COST_OPERATION == STD_OFF)
#define PDUR_ID_TxDiagP2P                        0
#define PDUR_ID2_TxDiagP2P                       0
#define PDUR_ID_RxDiagP2P                        1
#define PDUR_ID_TxDiagP2A                        2
#define PDUR_ID2_TxDiagP2A                       2
#define PDUR_ID_RxDiagP2A                        3
#define PDUR_ID_TxCan2LinDiag                    4
#define PDUR_ID2_TxCan2LinDiag                   4
#define PDUR_ID_RxCan2LinDiag                    5
#define PDUR_ID_TxLinDiag                        6
#define PDUR_ID2_TxLinDiag                       6
#define PDUR_ID_RxLinDiag                        7
#define PDUR_ID_TxMsgTime                        8
#define PDUR_ID2_TxMsgTime                       8
#define PDUR_ID_RxMsgAbsInfo                     9
#define PDUR_ID_SOAD_RX                          10
#define PDUR_ID_SOAD_TX                          11
#define PDUR_ID2_SOAD_TX                         11
#define PDUR_ID_J1939TP_TX                       12
#define PDUR_ID2_J1939TP_TX                      12
#define PDUR_ID_J1939TP_RX                       13
#define PDUR_ID_RxMsgFD                          14
#define PDUR_ID_TxMsgFD                          15
#define PDUR_ID2_TxMsgFD                         15
#define PDUR_ID_LIN_RX_MSG1                      16
#define PDUR_ID_LIN_TX_MSG1                      17
#define PDUR_ID2_LIN_TX_MSG1                     17

#else
#define PDUR_ID_TxDiagP2P                        CANTP_ID_TxDiagP2P
#define PDUR_ID2_TxDiagP2P                       DCM_ID_TxDiagP2P
#define PDUR_ID_RxDiagP2P                        DCM_ID_RxDiagP2P
#define PDUR_ID2_RxDiagP2P                       CANTP_ID_RxDiagP2P
#define PDUR_ID_TxDiagP2A                        CANTP_ID_TxDiagP2A
#define PDUR_ID2_TxDiagP2A                       DCM_ID_TxDiagP2A
#define PDUR_ID_RxDiagP2A                        DCM_ID_RxDiagP2A
#define PDUR_ID2_RxDiagP2A                       CANTP_ID_RxDiagP2A
#define PDUR_ID_TxCan2LinDiag                    CANTP_ID_TxCan2LinDiag
#define PDUR_ID2_TxCan2LinDiag                   LINTPGW_ID_TxCan2LinDiag
#define PDUR_ID_RxCan2LinDiag                    LINTPGW_ID_RxCan2LinDiag
#define PDUR_ID2_RxCan2LinDiag                   CANTP_ID_RxCan2LinDiag
#define PDUR_ID_TxLinDiag                        LINTP_ID_TxLinDiag
#define PDUR_ID2_TxLinDiag                       LINTPGW_ID_TxLinDiag
#define PDUR_ID_RxLinDiag                        LINTPGW_ID_RxLinDiag
#define PDUR_ID2_RxLinDiag                       LINTP_ID_RxLinDiag
#define PDUR_ID_TxMsgTime                        CANIF_ID_TxMsgTime
#define PDUR_ID2_TxMsgTime                       COM_ID_TxMsgTime
#define PDUR_ID_RxMsgAbsInfo                     COM_ID_RxMsgAbsInfo
#define PDUR_ID2_RxMsgAbsInfo                    CANIF_ID_RxMsgAbsInfo
#define PDUR_ID_SOAD_RX                          DCM_ID_SOAD_RX
#define PDUR_ID2_SOAD_RX                         SOADTP_ID_SOAD_RX
#define PDUR_ID_SOAD_TX                          SOADTP_ID_SOAD_TX
#define PDUR_ID2_SOAD_TX                         DCM_ID_SOAD_TX
#define PDUR_ID_J1939TP_TX                       J1939TP_ID_J1939TP_TX
#define PDUR_ID2_J1939TP_TX                      DCM_ID_J1939TP_TX
#define PDUR_ID_J1939TP_RX                       DCM_ID_J1939TP_RX
#define PDUR_ID2_J1939TP_RX                      J1939TP_ID_J1939TP_RX
#define PDUR_ID_RxMsgFD                          COM_ID_RxMsgFD
#define PDUR_ID2_RxMsgFD                         CANIF_ID_RxMsgFD
#define PDUR_ID_TxMsgFD                          CANIF_ID_TxMsgFD
#define PDUR_ID2_TxMsgFD                         COM_ID_TxMsgFD
#define PDUR_ID_LIN_RX_MSG1                      COM_ID_LIN_RX_MSG1
#define PDUR_ID2_LIN_RX_MSG1                     LINIF_ID_LIN_RX_MSG1
#define PDUR_ID_LIN_TX_MSG1                      LINIF_ID_LIN_TX_MSG1
#define PDUR_ID2_LIN_TX_MSG1                     COM_ID_LIN_TX_MSG1

#endif

#endif /* PDUR_PB_CFG_H_H */

