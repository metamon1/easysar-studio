* * *
"""본 저장소는 과학기술통신부의 재원으로 정보통신기획평가원(IITP)의 지원을 받아 (주) 드림에이스, 경북대학교, 대구경북과학기술원이 함께 수행한 "차량 ECU 응용소프트웨어 개발 및 검증자동화를 위한 가상 ECU기반 차량레벨 통합 시뮬레이션 기술개발(과제고유번호: 2710008421)" 과제의 일환으로 공개된 오픈소스 코드입니다. (2022.04~2024.12)"""

"""본 저장소는 가상 ECU 설정을 위한 Configuration 및 Generation 도구를 위한 것으로 가상 ECU에서 사용되는 Config 요소에 대해 수정, 삭제, 추가 등 작업을 진행하여 개발하고자 하는 가상 ECU에 맞는 구성 정보를 설정하는 용도로 활용됩니다."""
* * *

# Easysar Studio

## 시작하기
```
git clone git@gitlab.com:metamon1/easysar-studio.git
cd easysar-studio
git remote add origin https://gitlab.com/metamon1/easysar-studio.git
git branch -M main
```

## 1. OpenSAR STUDIO 실행
### 1.1 설치 lib
```sudo apt-get install libgtk-3-dev
sudo apt-get install libtool
sudo apt-get install libxkbcommon-x11-0
pip install PyQt5 == 5.7.1
```

### 1.2 구동
```
python3 studio.py
```  
진행 시 BSW 설정을 위한 STUDIO 창이 실행  

### 1.3 오류 현상
※ 오류 현상  
1) 초기 진입 시 J1939Tp 탭에서 좌상단에 있는 Add PduInfo 클릭 시 팅김 현상 발생 (아마 해당 섹션이 아닌 곳에서 추가를 하여 팅기는 것 같음)  
2) DBC 파일 입력 후 저장 또는 제네레이션 하지 않고, 비정상 종료를 하게되면 프로젝트가 먹통이되는 오류가 있음  

## 2. DBC 입출력

### 2.1 DBC 입출력 시 필요 lib
```
pip install ply
```

### 2.2 DBC 파일 동작 시퀀스
1) .dbc 파일 import  
2) py.can.database.access 폴더 내 python 파일을 통해 데이터 parsing  
( ↑ 비표준 용어가 여기서 걸러진다고 예상됨)  
3) 파싱된 데이터 OpenSAR Tool로 전송  
4) .dbc 데이터를 토대로 .arxml에 데이터 추가  
5) Argen 폴더 내 python 파일을 통해 데이터 Generation  
6) Generation 된 데이터를 _cfg.h c o, .xml 파일로 변환  
7) Virtual ECU 동작  

## ※ 코드 수정 사항 및 주의사항 ※

setMinimumWidth(self, **int**) 
특정 버전 이후 함수의 매개변수가 float형에서 int형으로 바뀐 것으로 보임

```
# /home/usr/autoas/com/as.tool/config.infrastructure.system/arxml/Argui.py

######전######
# line 528: 
self.frame.setMinimumWidth(self.width*3/5)
# line 578: 
self.table.setMinimumWidth(self.width()*3/4)
######후######
# line 528: 
self.frame.setMinimumWidth(self.width)
# line 578: 
self.table.setMinimumWidth(self.width())
```
##### 변경 시 문제점
정상적으로 구동은 가능하나 탭 선택 시 좌측 박스의 크기 제어에 어려움이 있음

import 부분 오류로 상대 경로 설정
GenRte.py

### 주의사항
autosar 패키지 설치 금지
```
pip3 uninstall autosar
```
